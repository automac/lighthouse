if git remote | grep upstream > /dev/null; then
    git remote add upsteam git@github.com:nuwave/lighthouse.git
fi
if [ -z "$1" ]; then
    echo 'you must enter a tag to update from, e.g.'
    echo '    ./update.sh v4.7.2'
    exit 1
fi
git pull upsteam $1