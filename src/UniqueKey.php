<?php

namespace Nuwave\Lighthouse;

use Illuminate\Database\Eloquent\Model;

class UniqueKey
{
    public static function get(Model $model)
    {
        if (method_exists($model, 'getUniqueKey')) {
            return $model->getUniqueKey();
        }
        return $model->getKey();
    }
}
